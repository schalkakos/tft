<?php

$servername = "localhost";
$username = "root";
$password = "";
$db_name="tftclone";

// Create connection
//$conn = new mysqli($servername, $username, $password, $db_name);
$db = new PDO("mysql:host=$servername;dbname=$db_name",$username,$password);

if($_GET["name"]){
    $name=$_GET["name"];

    $champ_query = $db->prepare("SELECT c.champ_name,
    c.img_url,
    c.cost,
    c.hp_1,
    c.hp_2,
    c.hp_3,
    c.mana,
    c.Armor,
    c.MR,
    c.dmg_1,
    c.dmg_2,
    c.dmg_3,
    c.att_spd,
    c.Range,
    c.ab_name,
    c.ab_type,
    c.ab_url,
    c.ab_desc,
    c.ab_det_1,
    c.ab_det_val_1,
    c.ab_det_2,
    c.ab_det_val_2,
    c.ab_det_3,
    c.ab_det_val_3,
    s1.syn_name AS s1_name,
    s1.syn_type AS s1_type,
    s1.syn_url AS s1_url,
    s1.syn_desc AS s1_desc,
    s1.syn_t1_numb AS s1_t1_numb,
    s1.syn_t1_bonus AS s1_t1_bonus,
    s1.syn_t2_numb AS s1_t2_numb,
    s1.syn_t2_bonus AS s1_t2_bonus,
    s1.syn_t3_numb AS s1_t3_numb,
    s1.syn_t3_bonus AS s1_t3_bonus,
    s2.syn_name AS s2_name,
    s2.syn_type AS s2_type,
    s2.syn_url AS s2_url,
    s2.syn_desc AS s2_desc,
    s2.syn_t1_numb AS s2_t1_numb,
    s2.syn_t1_bonus AS s2_t1_bonus,
    s2.syn_t2_numb AS s2_t2_numb,
    s2.syn_t2_bonus AS s2_t2_bonus,
    s2.syn_t3_numb AS s2_t3_numb,
    s2.syn_t3_bonus AS s2_t3_bonus,
    s3.syn_name AS s3_name,
    s3.syn_type AS s3_type,
    s3.syn_url AS s3_url,
    s3.syn_desc AS s3_desc,
    s3.syn_t1_numb AS s3_t1_numb,
    s3.syn_t1_bonus AS s3_t1_bonus,
    s3.syn_t2_numb AS s3_t2_numb,
    s3.syn_t2_bonus AS s3_t2_bonus,
    s3.syn_t3_numb AS s3_t3_numb,
    s3.syn_t3_bonus AS s3_t3_bonus,
    s4.syn_name AS s4_name,
    s4.syn_type AS s4_type,
    s4.syn_url AS s4_url,
    s4.syn_desc AS s4_desc,
    s4.syn_t1_numb AS s4_t1_numb,
    s4.syn_t1_bonus AS s4_t1_bonus,
    s4.syn_t2_numb AS s4_t2_numb,
    s4.syn_t2_bonus AS s4_t2_bonus,
    s4.syn_t3_numb AS s4_t3_numb,
    s4.syn_t3_bonus AS s4_t3_bonus
    FROM champ_joint 
    LEFT JOIN champions AS c
    ON c.champ_id=champ_joint.champ_id 
    LEFT JOIN synergies AS s1
    ON  s1.syn_id=champ_joint.or_id_1
    LEFT JOIN synergies AS s2
    ON s2.syn_id=champ_joint.or_id_2
    LEFT JOIN synergies AS s3
    ON s3.syn_id=champ_joint.cl_id_1
    LEFT JOIN synergies AS s4
    ON s4.syn_id=champ_joint.cl_id_2
    WHERE c.champ_name= :champ_name");
    $champ_query->bindParam(":champ_name", $name);
    $champ_query->execute();
    $champ_response = $champ_query->fetchAll(PDO::FETCH_ASSOC);
};



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TF Tactics</title>
    <link rel="icon" href="images\general\Tft_icon.ico" type="image/ico">
    <link rel="stylesheet" type="text/css" href="styles/nav-bar.css">
    <link rel="stylesheet" type="text/css" href="styles/champion-detail.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
</head>
<body>  
<?php include("nav-bar.php");

echo '<main>
    <aside>
        <div class="champ-portrait">
            <div class="portrait-wrapper">
                <img class="champ-img" src='.$champ_response[0]["img_url"].' alt='.$champ_response[0]["champ_name"].'>
            </div>
            <h2 id="champ_name">'.$champ_response[0]["champ_name"].'</h2>
        </div>
        <div class="champ-stats">
            <div class="stats-header"><h3>Stats</h3></div>
            <ul class="stats-list">
                <li>
                    <span class="stats-list-title">Cost:</span>
                    <img id="coin" src="images/general/coin.png" alt="coin image">
                    <span>'.$champ_response[0]["cost"].'</span>
                </li>
                <li>
                    <span class="stats-list-title">Health:</span>
                    <span> '.$champ_response[0]["hp_1"].' / '.$champ_response[0]["hp_2"].' / '.$champ_response[0]["hp_3"].'</span>
                </li>
                <li>
                    <span class="stats-list-title">Mana:</span>
                    <span>'.$champ_response[0]["mana"].'</span>
                </li>
                <li>
                    <span class="stats-list-title">Armor:</span>
                    <span>'.$champ_response[0]["Armor"].'</span>
                </li>
                <li>
                    <span class="stats-list-title">Magic Resist:</span>
                    <span>'.$champ_response[0]["MR"].'</span>
                </li>
                <li>
                    <span class="stats-list-title">Dps:</span>
                    <span>'.$champ_response[0]["dmg_1"]*$champ_response[0]["att_spd"].' / '.$champ_response[0]["dmg_2"]*$champ_response[0]["att_spd"].' / '.$champ_response[0]["dmg_3"]*$champ_response[0]["att_spd"].'</span>
                </li>
                <li>
                    <span class="stats-list-title">Damage</span>
                    <span>'.$champ_response[0]["dmg_1"].' / '.$champ_response[0]["dmg_2"].' / '.$champ_response[0]["dmg_3"].'</span>
                </li>
                <li>
                    <span class="stats-list-title">Attack Speed:</span>
                    <span>'.$champ_response[0]["att_spd"].'</span>
                </li>
                <li>
                    <span class="stats-list-title">Crit Rate:</span>
                    <span>25%</span>
                </li>
                <li>
                    <span class="stats-list-title">Range:</span>
                    <span>'.$champ_response[0]["Range"].'</span>
                </li>
            </ul>
        </div>
    </aside>
    <div class="details">
        <div class="ability-header">
            <h1>Ability</h1>
        </div>
        <div class="ability">
            <img class="ability-img" src='.$champ_response[0]["ab_url"].' alt="'.$champ_response[0]["champ_name"].' ability">
            <div class="ability-details">  
                <div class="ability-title">
                    <div class="ability-name">
                        <h2>'.$champ_response[0]["ab_name"].'</h2>
                        <span>'.$champ_response[0]["ab_type"].'</span>
                    </div>
                    <div class="ability-cost">
                        <span>Mana cost:</span>
                        <span>'.$champ_response[0]["mana"].'</span>
                    </div>
                </div>
                <div class="ability-description">
                    <p>'.$champ_response[0]["ab_desc"].'</p>
                </div>
                <div class="ability-value">
                    <span>'.$champ_response[0]["ab_det_1"].'</span>
                    <span>'.$champ_response[0]["ab_det_val_1"].'</span>
                </div>';
                if($champ_response[0]["ab_det_2"]){
                echo '<div class="ability-value">
                    <span>'.$champ_response[0]["ab_det_2"].' </span>
                    <span>'.$champ_response[0]["ab_det_val_2"].'</span>
                </div>';
                }
                if($champ_response[0]["ab_det_3"]){
                    echo '<div class="ability-value">
                        <span>'.$champ_response[0]["ab_det_3"].'</span>
                        <span>'.$champ_response[0]["ab_det_val_3"].'</span>
                    </div>';
                }

            echo '</div>
        </div>
        <div class="synergies-header">
            <h1>Synergies</h1>
        </div>
        <div class="synergies">
            <div class="synergie">
                <img class="synergie-img" src='.$champ_response[0]["s1_url"].' alt="'.$champ_response[0]["s1_name"].' image">
                <div class="synergie-details"> 
                    <div class="synergie-title">
                        <h2>'.$champ_response[0]["s1_name"].'</h2>
                        <span>'.$champ_response[0]["s1_type"].'</span> 
                    </div>';
                    if($champ_response[0]["s1_desc"]){
                    echo '<div class="synergie-description">
                        <p>'.$champ_response[0]["s1_desc"].'</p>
                    </div>';
                    }
                    echo '<ul class="synergie-list">
                        <li class="synergie-value">
                            <span class="synergie-value-numb" id="first-bonus">'.$champ_response[0]["s1_t1_numb"].'</span>
                            <span class="synergie-value-bonus">'.$champ_response[0]["s1_t1_bonus"].'</span>
                        </li>';
                        if($champ_response[0]["s1_t2_numb"]){
                        echo '<li class="synergie-value">
                            <span class="synergie-value-numb" id="second-bonus">'.$champ_response[0]["s1_t2_numb"].'</span>
                            <span class="synergie-value-bonus">'.$champ_response[0]["s1_t2_bonus"].'</span>
                        </li>';
                        }
                        if($champ_response[0]["s1_t3_numb"]){
                        echo '<li class="synergie-value">
                            <span class="synergie-value-numb" id="third-bonus">'.$champ_response[0]["s1_t3_numb"].'</span>
                            <span class="synergie-value-bonus">'.$champ_response[0]["s1_t3_bonus"].'</span>
                        </li>';
                        }
                    echo'</ul>
                </div>
            </div>';

            if($champ_response[0]["s2_name"]){
                echo '<div class="synergies">
                <div class="synergie">
                    <img class="synergie-img" src='.$champ_response[0]["s2_url"].' alt="'.$champ_response[0]["s2_name"].' image">
                    <div class="synergie-details"> 
                        <div class="synergie-title">
                            <h2>'.$champ_response[0]["s2_name"].'</h2>
                            <span>'.$champ_response[0]["s2_type"].'</span> 
                        </div>';
                        if($champ_response[0]["s2_desc"]){
                            echo '<div class="synergie-description">
                                <p>'.$champ_response[0]["s2_desc"].'</p>
                            </div>';
                            }
                        echo '<ul class="synergie-list">
                            <li class="synergie-value">
                                <span class="synergie-value-numb" id="first-bonus">'.$champ_response[0]["s2_t1_numb"].'</span>
                                <span class="synergie-value-bonus">'.$champ_response[0]["s2_t1_bonus"].'</span>
                            </li>';
                            if($champ_response[0]["s2_t2_numb"]){
                            echo '<li class="synergie-value">
                                <span class="synergie-value-numb" id="second-bonus">'.$champ_response[0]["s2_t2_numb"].'</span>
                                <span class="synergie-value-bonus">'.$champ_response[0]["s2_t2_bonus"].'</span>
                            </li>';
                            }
                            if($champ_response[0]["s2_t3_numb"]){
                            echo '<li class="synergie-value">
                                <span class="synergie-value-numb" id="third-bonus">'.$champ_response[0]["s2_t3_numb"].'</span>
                                <span class="synergie-value-bonus">'.$champ_response[0]["s2_t3_bonus"].'</span>
                            </li>';
                            }
                        echo'</ul>
                    </div>
                </div>';
            }
            echo '<div class="synergie">
                <img class="synergie-img" src='.$champ_response[0]["s3_url"].' alt="'.$champ_response[0]["s3_name"].' image">
                <div class="synergie-details"> 
                    <div class="synergie-title">
                        <h2>'.$champ_response[0]["s3_name"].'</h2>
                        <span>'.$champ_response[0]["s3_type"].'</span> 
                    </div>';
                    if($champ_response[0]["s3_desc"]){
                    echo '<div class="synergie-description">
                        <p>'.$champ_response[0]["s3_desc"].'</p>
                    </div>';
                    }
                    echo '<ul class="synergie-list">
                        <li class="synergie-value">
                            <span class="synergie-value-numb" id="first-bonus">'.$champ_response[0]["s3_t1_numb"].'</span>
                            <span class="synergie-value-bonus">'.$champ_response[0]["s3_t1_bonus"].'</span>
                        </li>';
                        if($champ_response[0]["s3_t2_numb"]){
                        echo '<li class="synergie-value">
                            <span class="synergie-value-numb" id="second-bonus">'.$champ_response[0]["s3_t2_numb"].'</span>
                            <span class="synergie-value-bonus">'.$champ_response[0]["s3_t2_bonus"].'</span>
                        </li>';
                        }
                        if($champ_response[0]["s3_t3_numb"]){
                        echo '<li class="synergie-value">
                            <span class="synergie-value-numb" id="third-bonus">'.$champ_response[0]["s3_t3_numb"].'</span>
                            <span class="synergie-value-bonus">'.$champ_response[0]["s3_t3_bonus"].'</span>
                        </li>';
                        }
                    echo'</ul>
                </div>
            </div>';
            if($champ_response[0]["s4_name"]){
                echo '<div class="synergies">
                <div class="synergie">
                    <img class="synergie-img" src='.$champ_response[0]["s4_url"].' alt="'.$champ_response[0]["s4_name"].' image">
                    <div class="synergie-details"> 
                        <div class="synergie-title">
                            <h2>'.$champ_response[0]["s4_name"].'</h2>
                            <span>'.$champ_response[0]["s4_type"].'</span> 
                        </div>';
                        if($champ_response[0]["s4_desc"]){
                            echo '<div class="synergie-description">
                                <p>'.$champ_response[0]["s4_desc"].'</p>
                            </div>';
                            }
                        echo '<ul class="synergie-list">
                            <li class="synergie-value">
                                <span class="synergie-value-numb" id="first-bonus">'.$champ_response[0]["s4_t1_numb"].'</span>
                                <span class="synergie-value-bonus">'.$champ_response[0]["s4_t1_bonus"].'</span>
                            </li>';
                            if($champ_response[0]["s4_t2_numb"]){
                            echo '<li class="synergie-value">
                                <span class="synergie-value-numb" id="second-bonus">'.$champ_response[0]["s4_t2_numb"].'</span>
                                <span class="synergie-value-bonus">'.$champ_response[0]["s4_t2_bonus"].'</span>
                            </li>';
                            }
                            if($champ_response[0]["s4_t3_numb"]){
                            echo '<li class="synergie-value">
                                <span class="synergie-value-numb" id="third-bonus">'.$champ_response[0]["s4_t3_numb"].'</span>
                                <span class="synergie-value-bonus">'.$champ_response[0]["s4_t3_bonus"].'</span>
                            </li>';
                            }
                        echo'</ul>
                    </div>
                </div>';
            }


        '</div>
    </div>
</main>';

// print_r($champ_response);
 ?>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="scripts/nav-bar.js"></script>
<script src="scripts/champions.js"></script>
</body>
</html>