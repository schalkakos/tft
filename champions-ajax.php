<?php
$servername = "localhost";
$username = "root";
$password = "";
$db_name="tftclone";

// Create connection
//$conn = new mysqli($servername, $username, $password, $db_name);
$db = new PDO("mysql:host=$servername;dbname=$db_name",$username,$password);
/*if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}*/







if(isset($_GET["orderBy"])){
    $orderby=$_GET["orderBy"];
    $allowed_values=["champ_name","origin","tfclass","cost"];
    $is_valid_value=array_search($orderby,$allowed_values);
    if($is_valid_value){
        $orderby=$allowed_values[$is_valid_value];
    }else{
        $orderby="champ_name";
    };

    if($_GET["direction"]=="DESC"){
        $direction= "DESC";
    }else{
        $direction= "ASC";
    }

    $order_query = $db->prepare("SELECT champions.champ_name AS champ_name, 
    champions.img_url, 
    champions.cost AS cost, 
    s1.syn_name AS origin, 
    s1.syn_url AS origin_url ,
    s2.syn_name AS origin2, 
    s2.syn_url AS origin2_url, 
    s3.syn_name AS tfclass, 
    s3.syn_url AS tfclass_url, 
    s4.syn_name AS tfclass2, 
    s4.syn_url AS tfclass2_url
    FROM champ_joint 
    LEFT JOIN champions 
    ON champions.champ_id=champ_joint.champ_id 
    LEFT JOIN synergies AS s1
    ON  s1.syn_id=champ_joint.or_id_1
    LEFT JOIN synergies AS s2
    ON s2.syn_id=champ_joint.or_id_2
    LEFT JOIN synergies AS s3
    ON s3.syn_id=champ_joint.cl_id_1
    LEFT JOIN synergies AS s4
    ON s4.syn_id=champ_joint.cl_id_2
    ORDER BY  $orderby $direction");



    $order_query->execute();

    //$query->debugDumpParams();
    
    $order_response = $order_query->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode($order_response);
}

if(isset($_GET["synergie-name"])){
    $syn_value=$_GET["synergie-name"];
    $syn_query = $db->prepare("SELECT * FROM `synergies` WHERE syn_name= :synergieValue");
    $syn_query->bindParam(":synergieValue", $syn_value);
    $syn_query->execute();

    $syn_response = $syn_query->fetchAll(PDO::FETCH_ASSOC);
    $syn_id=$syn_response[0]["syn_id"];
    

    $other_champs_query= $db->prepare("SELECT champions.img_url FROM `champ_joint` LEFT JOIN champions ON champions.champ_id=champ_joint.champ_id WHERE or_id_1=:syn_id OR or_id_2=:syn_id OR cl_id_1=:syn_id OR cl_id_2=:syn_id");
    $other_champs_query->bindParam(":syn_id", $syn_id);
    $other_champs_query->execute();
    $other_champs_response= $other_champs_query->fetchAll(PDO::FETCH_ASSOC);
    $i=1;
    //$response_array=array_merge($syn_response[0],$other_champs_response[0]);
    foreach($other_champs_response as $val){
        $current_array_value=$val["img_url"];
        $syn_response[0]["other_img_url"]["img_url_".$i] = $current_array_value;
        $i++;
    }
    
    
    echo json_encode($syn_response);
}
