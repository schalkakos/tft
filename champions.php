<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TF Tactics</title>
    <link rel="icon" href="images\general\Tft_icon.ico" type="image/ico">
    <link rel="stylesheet" type="text/css" href="styles/nav-bar.css">
    <link rel="stylesheet" type="text/css" href="styles/champions.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
</head>
<body>  
<?php include("nav-bar.php");?>

<main>
    <aside>
        <div class="filter">
            <div class="filter-header">
                    <h2>Filters</h2>
                    <button class="reset-button" type="button">Reset</button>
            </div>
            <div class="filter-content">
                <div class="filter-option">
                    <div class="filter-title" >
                        <span class="filter-span" >Cost</span>
                        <img class="chevron" src="/images/general/down-chevron.svg" alt="">
                    </div>
                    <ul class="filter-items" val-type="cost" id="cost">
                        <li class="filter-item" name="1">
                            <span class="filter-span">1 Cost</span> 
                            <div class="circle"></div>
                        <li class="filter-item" name="2">
                            <span class="filter-span">2 Cost</span> 
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="3">
                            <span class="filter-span"> 3 Cost</span>
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="4">
                            <span class="filter-span">4 Cost</span>
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="5">
                            <span class="filter-span">5 Cost</span>
                            <div class="circle"></div>
                        </li>
                    </ul>
                </div>
                <div class="filter-option">
                    <div class="filter-title">
                        <span class="filter-span">Origin</span>
                        <img class="chevron" src="images/general/down-chevron.svg" alt="">
                    </div>
                        <ul class="filter-items" val-type="origin" id="origin">
                            <li class="filter-item" name="Celestial">
                                <img class="filter-synergie-img" src=" images/synergies/Celestial.png" alt="Celestial">
                                <span class="filter-span">Celestial</span>
                                <div class="circle"></div>
                            </li>
                            <li class="filter-item" name="Chrono">
                                <img class="filter-synergie-img" src=" images/synergies/Chrono.png" alt="Chrono">
                                <span class="filter-span">Chrono</span>
                                <div class="circle"></div>
                            </li>
                            <li class="filter-item" name="Cybernetic">
                                <img class="filter-synergie-img" src=" images/synergies/Cybernetic.png" alt="Cybernetic"> 
                                <span class="filter-span">Cybernetic</span>
                                <div class="circle"></div>
                            </li>
                            <li class="filter-item" name="Dark-Star">
                                <img class="filter-synergie-img" src=" images/synergies/DarkStar.png" alt="Dark Star"> 
                                <span class="filter-span">Dark Star</span>
                                <div class="circle"></div>
                            </li>
                            <li class="filter-item" name="Mech-Pilot">
                                <img class="filter-synergie-img" src=" images/synergies/MechPilot.png" alt="Mech Pilot"> 
                                <span class="filter-span">Mech Pilot</span>
                                <div class="circle"></div>
                                </li>
                            <li class="filter-item" name="Rebel">
                                <img class="filter-synergie-img" src=" images/synergies/Rebel.png" alt="Rebel">
                                <span class="filter-span">Rebel</span>
                                <div class="circle"></div>
                            </li>
                            <li class="filter-item" name="Space-Pirate">
                                <img class="filter-synergie-img" src=" images/synergies/SpacePirate.png" alt="Space Pirate">
                                <span class="filter-span">Space Pirate</span>
                                <div class="circle"></div>
                            </li>
                            <li class="filter-item" name="Star-Guardian">
                                <img class="filter-synergie-img" src=" images/synergies/StarGuardian.png" alt="Star Guardian">
                                <span class="filter-span">Star Guardian</span>
                                <div class="circle"></div>
                            </li>
                            <li class="filter-item" name="Valkyrie">
                                <img class="filter-synergie-img" src=" images/synergies/Valkyrie.png" alt="Valkyrie"> 
                                <span class="filter-span">Valkyrie</span>
                                <div class="circle"></div>
                            </li>
                            <li class="filter-item" name="Void">
                                <img class="filter-synergie-img" src=" images/synergies/Void.png" alt="Void">
                                <span class="filter-span">Void</span>
                                <div class="circle"></div>
                            </li>
                        </ul>
                </div>
                <div class="filter-option">
                    <div class="filter-title">
                        <span class="filter-span">Class</span>
                        <img class="chevron" src="images/general/down-chevron.svg" alt="">
                    </div>
                    <ul class="filter-items" val-type="tfclass" id="tfclass">
                        <li class="filter-item" name="Blademaster">
                            <img class="filter-synergie-img" src=" images/synergies/Blademaster.png" alt="Blademaster">
                            <span class="filter-span">Blademaster</span>
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="Blaster">
                            <img class="filter-synergie-img" src=" images/synergies/Blaster.png" alt="Blaster">
                            <span class="filter-span">Blaster</span>
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="Brawler">
                            <img class="filter-synergie-img" src=" images/synergies/Brawler.png" alt="Brawler">
                            <span class="filter-span">Brawler</span>
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="Demolitionist">
                            <img class="filter-synergie-img" src=" images/synergies/Demolitionist.png" alt="Demolitionist">
                            <span class="filter-span">Demolitionist</span>
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="Infiltrator">
                            <img class="filter-synergie-img" src=" images/synergies/Infiltrator.png" alt="Infiltrator">
                            <span class="filter-span">Infiltrator</span>
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="Mana-Reaver">
                            <img class="filter-synergie-img" src=" images/synergies/ManaReaver.png" alt="Mana-Reaver">
                            <span class="filter-span">Mana-Reaver</span>
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="Mercenary">
                            <img class="filter-synergie-img" src=" images/synergies/Mercenary.png" alt="Mercenary">
                            <span class="filter-span">Mercenary</span>
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="Mystic">
                            <img class="filter-synergie-img" src=" images/synergies/Mystic.png" alt="Mystic">
                            <span class="filter-span">Mystic</span>
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="Protector">
                            <img class="filter-synergie-img" src=" images/synergies/Protector.png" alt="Protector">
                            <span class="filter-span">Protector</span>
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="Sniper">
                            <img class="filter-synergie-img" src=" images/synergies/Sniper.png" alt="Sniper">
                            <span class="filter-span">Sniper</span>
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="Sorcerer">
                            <img class="filter-synergie-img" src=" images/synergies/Sorcerer.png" alt="Sorcerer">
                            <span class="filter-span">Sorcerer</span>
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="Starship">
                            <img class="filter-synergie-img" src=" images/synergies/Starship.png" alt="Starship">
                            <span class="filter-span">Starship</span>
                            <div class="circle"></div>
                        </li>
                        <li class="filter-item" name="Vanguard">
                            <img class="filter-synergie-img" src=" images/synergies/Vanguard.png" alt="Vanguard">
                            <span class="filter-span">Vanguard</span>
                            <div class="circle"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div> 
    </aside>      
    <div class="content">
        <div class="content-header">
            <div class="content-header-wrapper">
                <h2 class="content-header-title">Teamfight Tactics Champions</h2>
                <div class="search-bar-wrapper">
                    <img class="search-bar-icon" src="images/general/search.svg">
                    <input type="text" class="search-bar" placeholder="Search by name, origin, or class...">
                </div>
            </div>
            <div class="display-filters">

            </div>
        </div>
        <div class="content-list-wrapper">
            <div class="content-list-titles">
                <div class="list-title active-sort" id="champ-name" sort-value="champ_name">
                    <span>Champion</span>
                    <img class="sort-img-down active-sort-img" src="images/general/sort-down.svg" sort-type="ASC">
                    <img class="sort-img-up" src="images/general/sort-up.svg" sort-type="DESC">
                </div>
                <div class="list-title" id="champ-origin" sort-value="origin">
                    <span>Origin</span>
                    <img class="sort-img-down" src="images/general/sort-down.svg" sort-type="ASC">
                    <img class="sort-img-up" src="images/general/sort-up.svg" sort-type="DESC">
                </div>
                <div class="list-title" id="champ-class" sort-value="tfclass">
                    <span>Class</span>
                    <img class="sort-img-down" src="images/general/sort-down.svg" sort-type="ASC">
                    <img class="sort-img-up" src="images/general/sort-up.svg" sort-type="DESC">
                </div>
                <div class="list-title" id="champ-cost" sort-value="cost">
                    <span>Cost</span>
                    <img class="sort-img-down" src="images/general/sort-down.svg" sort-type="ASC">
                    <img class="sort-img-up" src="images/general/sort-up.svg" sort-type="DESC">
                </div>
            </div>
            <div class="content-list">
            <?php include("get-champions.php");?>

        </div>

    </div>
</main>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="scripts/nav-bar.js"></script>
<script src="scripts/champions.js"></script>
</body>
</html>

