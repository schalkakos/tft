<?php

class Character {
  private $db;

  public function __construct(){
    include("connect.php");
    $this->db = new PDO("mysql:host=$servername;dbname=$db_name",$username,$password);


  }

  public function load($character_id){

    $query = $this->db->prepare("SELECT *
    FROM champions 
    WHERE champ_id=:champId");
    $query->bindParam(":champId", $character_id);
    $query->execute();
    $result = $query -> fetchAll(PDO::FETCH_ASSOC);

    foreach($result[0] as $k=>$v){
        $this->$k = $v;
    }

    return $this;
  }

  public function loadLess($character_id){

    $query = $this->db->prepare("SELECT 
    champ_name,
    img_url
    FROM champions 
    WHERE champ_id=:champId");
    $query->bindParam(":champId", $character_id);
    $query->execute();
    $result = $query -> fetchAll(PDO::FETCH_ASSOC);
  
    foreach($result[0] as $k=>$v){
        $this->$k = $v;
    }
  
    return $this;
  }

  public function loadWithSynergy($champ_joint_id, $less_data = false){
    include("synergy.class.php");

    $query = $this->db->prepare("SELECT 
    champ_id,
    or_id_1,
    or_id_2,
    cl_id_1,
    cl_id_2
    FROM champ_joint 
    WHERE id=:champJointId");
    $query->bindParam(":champJointId", $champ_joint_id);
    $query->execute();
    $result = $query -> fetchAll(PDO::FETCH_ASSOC);   

    $result_keys = array_keys($result[0]);

    if($less_data){
      $this->loadLess($result[0]["champ_id"]);
    }else{
      $this->load($result[0]["champ_id"]);
    }

    $e = 1;
    for($i = 1; $i <= 4; $i++){
      $synergy = new Synergy();
      if($less_data & strlen($result[0][$result_keys[$i]]) >= 1){
        $this->synergies[$e] = $synergy->loadLess( $result[0][$result_keys[$i]] );
        $e++;
      }else if(!$less_data & strlen($result[0][$result_keys[$i]]) >= 1){
        $this->synergies[$e] = $synergy->load( $result[0][$result_keys[$i]] );
        $e++;
      }
    }
    return $this;
  }

  public function loadWithItem($team_comp_item_id, $less_data = false){
    include("item.class.php");

    $query = $this->db->prepare("SELECT *
    FROM team_comp_item_joint 
    WHERE team_comp_item_id=:teamCompItemId");
    $query->bindParam(":teamCompItemId", $team_comp_item_id);
    $query->execute();
    $result = $query -> fetchAll(PDO::FETCH_ASSOC);

    if($less_data){
      $this->loadLess($result[0]["champion_id"]);
    }else{
      $this->load($result[0]["champion_id"]);
    }

    for($i = 1; $i <= 3; $i++){
      $item = new Item();
      if($result[0]["item_id_".$i]){
        $this->items[$i] = $item->load($result[0]["item_id_".$i]);
      }
    }

    return $this;

  }

 
}


