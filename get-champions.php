<?php
$servername = "localhost";
$username = "root";
$password = "";
$db_name="tftclone";

// Create connection
$conn = new mysqli($servername, $username, $password, $db_name);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$query="SELECT champions.champ_name AS champ_name, 
champions.img_url, 
champions.cost AS cost, 
s1.syn_name AS origin, 
s1.syn_url AS origin_url ,
s2.syn_name AS origin2, 
s2.syn_url AS origin2_url, 
s3.syn_name AS tfclass, 
s3.syn_url AS tfclass_url, 
s4.syn_name AS tfclass2, 
s4.syn_url AS tfclass2_url
FROM champ_joint 
LEFT JOIN champions 
ON champions.champ_id=champ_joint.champ_id 
LEFT JOIN synergies AS s1
ON  s1.syn_id=champ_joint.or_id_1
LEFT JOIN synergies AS s2
ON s2.syn_id=champ_joint.or_id_2
LEFT JOIN synergies AS s3
ON s3.syn_id=champ_joint.cl_id_1
LEFT JOIN synergies AS s4
ON s4.syn_id=champ_joint.cl_id_2
ORDER BY champ_name";


$result = $conn->query($query);

if ($result->num_rows > 0) {
    // output data of each row
    while($champ_row = $result->fetch_assoc()) {
        $needle = " ";
        $search_term = "";
        if(strpos($champ_row["champ_name"], $needle)){
            $search_term = str_replace(" ", "-", $champ_row["champ_name"]);
        }else{
            $search_term = $champ_row["champ_name"];
        }
        
        echo '<div class="champ-wrapper">
        <div class="name" search-term='.$search_term.'>
        
            <a href="champion-detail.php?name='.$champ_row["champ_name"].'" class="champ-details-page">
                <img class="champ-img" src='.$champ_row["img_url"].' >
                <span>'.$champ_row["champ_name"].'</span>
            </a>

        </div>';
        if(strpos($champ_row["origin"], $needle)){
            $search_term = str_replace(" ", "-", $champ_row["origin"]);
        }else{
            $search_term = $champ_row["origin"];
        }
        echo '<div class="origin">
            <div class="origin-sub" search-term='. $search_term.'>
                <img class="champ-origin-img" src='.$champ_row["origin_url"].'>
                <span class="champ-wrapper-span">'.$champ_row["origin"].'</span>
            </div>';
            if(strpos($champ_row["origin2"], $needle)){
                $search_term = str_replace(" ", "-", $champ_row["origin2"]);
            }else{
                $search_term = $champ_row["origin2"];
            }
            if($champ_row["origin2"]){
            echo '<div class="origin-sub" search-term='. $search_term.'>
                <img class="champ-origin-img" src='.$champ_row["origin2_url"].' >
                <span class="champ-wrapper-span">'.$champ_row["origin2"].'</span>
            </div>';
            }
            if(strpos($champ_row["tfclass"], $needle)){
                $search_term = str_replace(" ", "-", $champ_row["tfclass"]);
            }else{
                $search_term = $champ_row["tfclass"];
            }
         echo '</div>
        <div class="tfclass">
            <div class="tfclass-sub" search-term='. $search_term.'>
                <img class="champ-tfclass-img" src='.$champ_row["tfclass_url"].'>
                <span class="champ-wrapper-span">'.$champ_row["tfclass"].'</span>
            </div>';
            if(strpos($champ_row["tfclass2"], $needle)){
                $search_term = str_replace(" ", "-", $champ_row["tfclass2"]);
            }else{
                $search_term = $champ_row["tfclass2"];
            }
            if($champ_row["tfclass2"]){
            echo '<div class="tfclass-sub" search-term='.$search_term.'>
                <img class="champ-tfclass-img" src='.$champ_row["tfclass2_url"].' >
                <span class="champ-wrapper-span">'.$champ_row["tfclass2"].'</span>
            </div>';
            }
         echo '</div>
        <div class="cost-sub" search-term='.$champ_row["cost"].'>
                <img class="coin" src="images/general/coin.png">
                <span class="champ-wrapper-span">'.$champ_row["cost"].'</span>
        </div>
    </div>';
    }
} else {
    echo "0 results";
}
$conn->close();
?>