
    
<?php
   $servername = "localhost";
   $username = "root";
   $password = "";
   $db_name="tftclone";
   
   // Create connection
   $db = new PDO("mysql:host=$servername;dbname=$db_name",$username,$password);
    
    
   $item_query = $db->prepare("SELECT 
   basic_1.item_url as basic_1_img, 
   basic_2.item_url as basic_2_img, 
   finished.item_url as finished_img, 
   finished.item_desc as finished_desc
    FROM item_joint
    LEFT JOIN items AS basic_1
    ON item_joint.basic_item_id_1=basic_1.item_id
    LEFT JOIN items AS basic_2
    ON item_joint.basic_item_id_2=basic_2.item_id
    LEFT JOIN items AS finished
    ON item_joint.finished_item_id=finished.item_id
    WHERE basic_item_id_1='1'");
    $item_query->execute();
    $result= $item_query -> fetchAll(PDO::FETCH_ASSOC);
    foreach ($result as $i => $array) {
        echo '<div class="item-wrapper">    
                <div class="recipe">
                    <img class="result-img" src='.$array["basic_1_img"].'>
                    <img class="result-img" src='.$array["basic_2_img"].'>
                </div>
                <div class="result">
                    <img class="result-img" src='.$array["finished_img"].' alt="">
                </div>
                <div class="description">
                    <p>'.$array["finished_desc"].'</p>
                </div>
            </div>';
    };