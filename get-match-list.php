<?php
$api_key = "RGAPI-93709a7f-6c7a-4691-9b9b-ab8508b82f94";

 if(isset($_GET["puuid"])){

    $puuid = $_GET["puuid"];
    $region = $_GET["region"];
    
   

   if($region === "br1" || $region === "na1" || $region === "la1" || $region === "la2" || $region === "oc1"){
         $region2 = "americas";
   }else if($region === "eun1" || $region === "euw1" || $region === "tr1" || $region === "ru"){
         $region2 = "europe";
   }else if($region === "jp1" || $region === "kr"){
         $region2 = "asia";
   }
   

   $url = "https://".$region2.".api.riotgames.com/tft/match/v1/matches/by-puuid/".$puuid."/ids?count=7&api_key=";
   $request_url = $url.$api_key;
   $curl = curl_init($request_url);
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
   $response_json = curl_exec($curl);
   $match_array = json_decode($response_json);
   

  


    

   if(!$match_array){
       echo json_encode("No matches found");
   }else{
      $match_list = "";
      foreach($match_array as $val){
         // $val = $match_array[0]; 


         $url = "https://americas.api.riotgames.com/tft/match/v1/matches/".$val."?api_key=";
         $request_url = $url.$api_key;
         $curl = curl_init($request_url);
         curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
         $response_json = curl_exec($curl);
         $response =  json_decode($response_json);
         $game_time = $response->info->game_length;
         
         // echo json_encode($response);
        


           



           
           foreach($response->info->participants as $value){
               if($value->puuid === $puuid){
                   $placement = $value->placement;
                   $level = "Level ".$value->level;
                   $placement_css_class = "";

                   if($placement === 1){
                       $placement_css_class = "gold";
                   }else if($placement === 2){
                       $placement_css_class = "silver";
                   }else if($placement === 3){
                       $placement_css_class = "bronze";
                   }else{
                       $placement_css_class = "iron";
                   }

                   $match_list = $match_list.'<div class="match-wrapper">
                       <div class="side-bar side-bar-'.$placement_css_class.'"></div>
                       <div class="game-info">
                       <h2 class="placement-'.$placement_css_class.'">#'.$placement.'</h2>';

                   $match_list = $match_list.'<span>Ranked</span>
                                           <span>'.gmdate("i:s", $game_time).'</span>
                                           <span>'.$level.'</span>
                                           </div>
                                           <div class="details">
                                               <div class="traits-list">';

                   foreach($value->traits as $tier){
                       $tier_total = $tier->tier_total;
                       $current_tier = $tier->tier_current;
                       if($tier_total === 1){
                           if($current_tier == 1){
                              $tier->style = 1;
                           }
                       }else if($tier_total === 2){
                           if($current_tier === 1){
                              $tier->style = 3;
                           }else if($current_tier === 2){
                              $tier->style = 1;
                           }
                       }else if($tier_total === 3){
                           if($current_tier === 1){
                              $tier->style = 3;
                           }else if($current_tier === 2){
                              $tier->style = 2;
                           }else if($current_tier === 3){
                              $tier->style = 1;
                           }
                       }else if($tier_total === 4){
                           if($current_tier === 1){
                              $tier->style = 4;
                           }else if($current_tier === 2){
                              $tier->style = 3;
                           }else if($current_tier === 3){
                              $tier->style = 2;
                           }else if($current_tier === 4){
                              $tier->style = 1;
                           }
                       }

                   }

                   usort($value->traits, function($a, $b){
                       if($a->style < $b->style){
                           return -1;
                       }else if($a->style > $b->style){
                           return 1;
                       }
                       return 0;
                   });

                   foreach($value->traits as $ind_trait){

                       $tier_total = $ind_trait->tier_total;
                       $current_tier = $ind_trait->tier_current;
                       $name = $ind_trait->name;
                       $needle = "_";
                       $trait_css_class = "";
                       if(strpos($name, $needle)){
                           $name = explode("_", $name);
                           $name = $name[1];
                       }
                       if($tier_total === 1){
                           if($current_tier === 1){
                               $trait_css_class = "third";
                           }else if($current_tier === 0){
                               continue;
                           }
                       }else if($tier_total === 2){
                           if($current_tier === 1){
                               $trait_css_class = "first";
                           }else if($current_tier === 2){
                               $trait_css_class = "third";
                           }else if($current_tier === 0){
                               continue;
                           }
                       }else if($tier_total === 3 || $tier_total === 4){
                           if($current_tier === 1){
                               $trait_css_class = "first";
                           }else if($current_tier === 2){
                               $trait_css_class = "second";
                           }else if($current_tier === 3){
                               $trait_css_class = "third";
                           }else if($current_tier === 4){
                               $trait_css_class = "forth";
                           }else if($current_tier === 0){
                               continue;
                           }
                       }

                       $match_list = $match_list.'<div class="trait '.$trait_css_class.'-bonus">
                                                           <img class="trait-img" src = "images/synergies/'.$name.'.png" >
                                               </div>';
                   }

                   $match_list = $match_list.'</div>
                   <div class="champions-list">';

               
                   foreach($value->units as $unit){
                       $star = $unit->tier;
                       $character_id = explode("_",$unit->character_id);
                     
                       $match_list = $match_list.'<div class = "champion">
                       <img class="champ_img" src="images/champions/'.$character_id[1].'.png" >
                       <img class="stars" src="images/general/stars'.$star.'.png" >
                   </div>';

                   }
                   $match_list = $match_list.' 
                           </div>
                       </div>    
                   </div>';
           
                   
               };
           }
           
        

      }
    echo json_encode($match_list);
    // echo json_encode("answer from php");

   }
 }