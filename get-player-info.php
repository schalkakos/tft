<?php
$field_value = $_GET["username"];
$region1 = $_GET["region"];



if(strpos($field_value, " ")){
    $display_name = str_replace(' ', '%20', $field_value);
}else{
    $display_name = $field_value;
}

$url = "https://".$region1.".api.riotgames.com/tft/summoner/v1/summoners/by-name/".$display_name."?api_key=";
$api_key = "RGAPI-93709a7f-6c7a-4691-9b9b-ab8508b82f94";
$request_url = $url.$api_key;
$curl = curl_init($request_url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$response_json = curl_exec($curl);
$response = json_decode($response_json);

if(isset($response->id)){
    $summoner_id = $response->id;
    $puuid = $response->puuid;

    $url = "https://".$region1.".api.riotgames.com/tft/league/v1/entries/by-summoner/".$summoner_id."?api_key=";
    $request_url = $url.$api_key;
    $curl = curl_init($request_url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response_json = curl_exec($curl);
    $rank_response = json_decode($response_json);
    
    if(!$rank_response){
        $display_tier = "Unranked";
        $rank = "";
        $rank_img_url = "images/ranking/unranked.png";     
    }else{
        $tier = $rank_response[0]->tier;
        $tier = strtolower($tier);
        $display_tier = ucfirst($tier);
    
        $rank = $rank_response[0]->rank;
        $league_points = $rank_response[0]->leaguePoints;
        $rank_img_url = "images/ranking/".$tier."_".$rank.".png";
    }


}else{
    $display_tier = "Unranked";
    $rank = "";
    $rank_img_url = "images/ranking/unranked.png";
    $match_array = ""; 
}
