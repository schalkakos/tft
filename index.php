<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TF Tactics</title>
    <link rel="icon" href="images\general\Tft_icon.ico" type="image/ico">
    <link rel="stylesheet" type="text/css" href="styles/nav-bar.css">
    <link rel="stylesheet" type="text/css" href="styles/index.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
</head>
<body>
    <?php include("nav-bar.php");?>
    <main>
        <h1>TF Tactics</h1>
        <h3></h3>

        <form class="search" action="match-history.php" method="GET">
            <select name="region" id="region">
                    <option value="br1">BR</option>
                    <option value="eun1">EUNE</option>
                    <option value="euw1">EUW</option>
                    <option value="jp1">JP</option>
                    <option value="kr">KR</option>
                    <option value="la1">LAN</option>
                    <option value="la1">LAS</option>
                    <option value="na1" selected>NA</option>
                    <option value="oc1">OCE</option>
                    <option value="tr1">TR</option>
                    <option value="ru">RU</option>    
            </select>
            <input class="input-field" name="username" type="text" placeholder="Search Summoner Name">
            <button type="submit">Search</button>
        </form>
    </main>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="scripts/nav-bar.js"></script>
</body>
</html>