<?php
$servername = "localhost";
    $username = "root";
    $password = "";
    $db_name="tftclone";
    
    $db = new PDO("mysql:host=$servername;dbname=$db_name",$username,$password);

if(isset($_GET["item-id"])){
    $item_id=$_GET["item-id"];
    $item_type=$_GET["item-type"];

    
     
    if($item_type==="base"){;
        $item_query = $db->prepare("(SELECT 
        basic_1.item_url as basic_1_img, 
        basic_2.item_url as basic_2_img, 
        finished.item_url as finished_img, 
        finished.item_desc as finished_desc
        FROM item_joint
        LEFT JOIN items AS basic_1
        ON item_joint.basic_item_id_1=basic_1.item_id
        LEFT JOIN items AS basic_2
        ON item_joint.basic_item_id_2=basic_2.item_id
        LEFT JOIN items AS finished
        ON item_joint.finished_item_id=finished.item_id
        WHERE basic_item_id_1=:itemid)
        UNION
        (SELECT 
        basic_2.item_url as basic_2_img,
 		basic_1.item_url as basic_1_img,  
        finished.item_url as finished_img, 
        finished.item_desc as finished_desc
        FROM item_joint
        LEFT JOIN items AS basic_1
        ON item_joint.basic_item_id_1=basic_1.item_id
        LEFT JOIN items AS basic_2
        ON item_joint.basic_item_id_2=basic_2.item_id
        LEFT JOIN items AS finished
        ON item_joint.finished_item_id=finished.item_id
        WHERE basic_item_id_2=:itemid)");
        $item_query->bindParam(":itemid", $item_id);
        $item_query->execute();
        $response= $item_query -> fetchAll(PDO::FETCH_ASSOC);
    }else if($item_type==="finished"){
        $item_query = $db->prepare("SELECT 
        basic_1.item_url as basic_1_img, 
        basic_2.item_url as basic_2_img, 
        finished.item_url as finished_img, 
        finished.item_desc as finished_desc
        FROM item_joint
        LEFT JOIN items AS basic_1
        ON item_joint.basic_item_id_1=basic_1.item_id
        LEFT JOIN items AS basic_2
        ON item_joint.basic_item_id_2=basic_2.item_id
        LEFT JOIN items AS finished
        ON item_joint.finished_item_id=finished.item_id
        WHERE finished_item_id=:itemid");
        $item_query->bindParam(":itemid", $item_id);
        $item_query->execute();
        $response= $item_query -> fetchAll(PDO::FETCH_ASSOC);
    }
    echo json_encode($response);

}

if(isset($_GET["hover-item-id"])){
    $hover_item_id=$_GET["hover-item-id"];
    $hover_item_type=$_GET["hover-item-type"];
    $item_query = $db->prepare(
        "SELECT 
        item_name,
        item_url,
        item_bonus_1,
        item_bonus_2,
        item_desc 
        FROM items
        WHERE item_id=:itemid");
        $item_query->bindParam(":itemid", $hover_item_id);
        $item_query->execute();
        $response= $item_query -> fetchAll(PDO::FETCH_ASSOC);


    if($hover_item_type==="base"){;
        $item_query = $db->prepare(
        "(SELECT finished.item_url as finished_img 
        FROM item_joint
        LEFT JOIN items AS basic_1
        ON item_joint.basic_item_id_1=basic_1.item_id
        LEFT JOIN items AS basic_2
        ON item_joint.basic_item_id_2=basic_2.item_id
        LEFT JOIN items AS finished
        ON item_joint.finished_item_id=finished.item_id
        WHERE basic_item_id_1=:itemid)
        UNION
        (SELECT finished.item_url as finished_img 
        FROM item_joint
        LEFT JOIN items AS basic_1
        ON item_joint.basic_item_id_1=basic_1.item_id
        LEFT JOIN items AS basic_2
        ON item_joint.basic_item_id_2=basic_2.item_id
        LEFT JOIN items AS finished
        ON item_joint.finished_item_id=finished.item_id
        WHERE basic_item_id_2=:itemid)");
        $item_query->bindParam(":itemid", $hover_item_id);
        $item_query->execute();
        $recipe_response= $item_query -> fetchAll(PDO::FETCH_ASSOC);
        $i=1;
        foreach ($recipe_response as $index => $val) {
            foreach ($recipe_response[$index] as $key => $value) {
                $response[0]["item_recipe"][$key."_".$i] = $value;
                $i++;
            }
        }
    }else if($hover_item_type==="finished"){
        $item_query = $db->prepare("SELECT 
        basic_1.item_url as basic_1_img, 
        basic_2.item_url as basic_2_img
        FROM item_joint
        LEFT JOIN items AS basic_1
        ON item_joint.basic_item_id_1=basic_1.item_id
        LEFT JOIN items AS basic_2
        ON item_joint.basic_item_id_2=basic_2.item_id
        LEFT JOIN items AS finished
        ON item_joint.finished_item_id=finished.item_id
        WHERE finished_item_id=:itemid");
        $item_query->bindParam(":itemid", $hover_item_id);
        $item_query->execute();
        $recipe_response= $item_query -> fetchAll(PDO::FETCH_ASSOC);
        foreach ($recipe_response[0] as $key => $value) {
            $response[0]["item_recipe"][$key] = $value;
        }
    }
    echo json_encode($response);
}