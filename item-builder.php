<?php
   $servername = "localhost";
   $username = "root";
   $password = "";
   $db_name="tftclone";
   
   // Create connection
   $db = new PDO("mysql:host=$servername;dbname=$db_name",$username,$password);
    
    
   $item_query = $db->prepare("SELECT item_id,item_type,item_url
    FROM items 
    ORDER BY item_type");
    $item_query->execute();
    $result= $item_query -> fetchAll(PDO::FETCH_ASSOC);
    $base_items="";
    $finished_items="";
    foreach ($result as $i => $array) {
        if($array["item_type"]=="base"){
            $base_items=$base_items."<div class='item-img' id='".$array["item_id"]."' type='".$array["item_type"]."'>
                            <img src='".$array["item_url"]."'>
                        </div>";
        }else if($array["item_type"]=="finished"){
            $finished_items=$finished_items."<div class='item-img' id='".$array["item_id"]."' type='".$array["item_type"]."'>
                                <img src='".$array["item_url"]."'>
                            </div>";
        }
    };
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TF Tactics</title>
    <link rel="icon" href="images\general\Tft_icon.ico" type="image/ico">
    <link rel="stylesheet" type="text/css" href="styles/nav-bar.css">
    <link rel="stylesheet" type="text/css" href="styles/item-builder.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
</head>
<body>  
<?php include("nav-bar.php");?>

<main>

<div class="content">
     <div class="item-lists">
        
        <div class="base-items">
            <div class="content-header">
                <h1 class="content-header-title">Item Builder</h1>
                <p class="content-header-description">Chose one of the base items to see all the possible combinations or chose a finished item to see there recipe.</p>
            </div>
            <div class="base-list">
                <?php echo $base_items ?>
            </div>
        </div>
        <aside>
            <div class="finished-title">
            <h2>Combined Items</h2>
            </div>
            <div class="finished-list">
                <?php echo $finished_items ?>
            </div>
        </aside>
    </div>
    <div class="content-list" id="list"> 
        <div class="list-header">
            <div class="list-header-recipe">
                <span>Recipe</span>
            </div>
            <div class="list-header-result">
                <span>Result Item</span>
            </div>
            <div class="list-header-description">
                <span>Description</span>
            </div>
        </div>
        <div class="item-builder-list">
            <?php include("get-items.php") ?>
            
        </div>
    </div>  
</div>
</main>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="scripts/nav-bar.js"></script>
<script src="scripts/item-builder.js"></script>
</body>
</html>
