<?php

 class Item {
   private $db;

   public function __construct(){
    include("connect.php");
    $this->db = new PDO("mysql:host=$servername;dbname=$db_name",$username,$password);

    
}

   public function load($item_id){

    $query = $this->db->prepare("SELECT *
    FROM items 
    WHERE item_id=:itemId");
    $query->bindParam(":itemId", $item_id);
    $query->execute();
    $result = $query -> fetchAll(PDO::FETCH_ASSOC);
    foreach($result[0] as $k=>$v){

        $this->$k = $v;
    }
    return $this;

   }

    public function loadLess($item_id){
        $query = $this->db->prepare("SELECT 
        item_name,
        item_url,
        item_type
        FROM items 
        WHERE item_id=:itemId");
        $query->bindParam(":itemId", $item_id);
        $query->execute();
        $result = $query -> fetchAll(PDO::FETCH_ASSOC);
        foreach($result[0] as $k=>$v){

            $this->$k = $v;
        }
        return $this;
    }

}

