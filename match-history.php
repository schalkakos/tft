<?php
    
    // NOTE: make it so session keeps the region and username with both index.php and match-history.php


    if(!empty($_GET["username"]) && !empty($_GET["region"])){

        include("get-player-info.php");    
       
}else{
    header("Location: index.php");
}
   
?>

<!DOCTYPE html>
<html lang = "en">
<head>
    <meta charset = "UTF-8">
    <meta name = "viewport" content = "width = device-width, initial-scale = 1.0">
    <meta http-equiv = "X-UA-Compatible" content = "ie = edge">
    <title>TF Tactics</title>
    <link rel = "icon" href = "images\general\Tft_icon.ico" type = "image/ico">
    <link rel = "stylesheet" type = "text/css" href = "styles/nav-bar.css">
    <link rel = "stylesheet" type = "text/css" href = "styles/match-history.css">
    <link href = "https://fonts.googleapis.com/css?family = Roboto:300,400,700&display = swap" rel = "stylesheet">
</head>
<body>
<?php include("nav-bar.php");?>

<div class="search-username">
    <form class="search" action="match-history.php" method="GET">
                <select name="region" id="region">
                        <option value="br1">BR</option>
                        <option value="eun1">EUNE</option>
                        <option value="euw1">EUW</option>
                        <option value="jp1">JP</option>
                        <option value="kr">KR</option>
                        <option value="la1">LAN</option>
                        <option value="la1">LAS</option>
                        <option value="na1" selected>NA</option>
                        <option value="oc1">OCE</option>
                        <option value="tr1">TR</option>
                        <option value="ru">RU</option>    
                </select>
                <input class="input-field" name="username" type="text" placeholder="Search Summoner Name">
                <button type="submit">Search</button>
    </form>
</div>
<main>
<aside>
    <div class = "rank-img">
        <img src =<?php echo $rank_img_url ?> alt = "">
    </div>
    <div class = "rank-info">
        <h2><?php echo $field_value; ?></h2>
        <div class = "rank-details">
        
             <div class="bottom-bar diamond"></div>

        <?php

        if(isset($response->id)){

            echo '<span class="rank '.$tier.'-font">'.$display_tier." ".$rank.'</span>
            <span class="lp">'.$league_points.' lp</span>
            <div class="bottom-bar '.$tier.'"></div>';
         }else{
            echo '<span class="rank">'.$display_tier.'</span>';
         }   ?>
        </div>
    </div>
    <p class="match-list-assist username"><?php if(isset($puuid)){ echo $puuid;}?></p>
    <p class="match-list-assist region"><?php if(isset($region1)){echo $region1;} ?></p>
</aside>
<div class = "content">
    <img class="loading loading-dn" src="images/general/loading.svg" >
    
    

    <?php
    // if(!$match_array){
    //     echo "<span class='no-match'>No matches found</span>";
    // }else{
    //     foreach($match_array as $val){
    //         $match_list = "";
    //         $url = "https://americas.api.riotgames.com/tft/match/v1/matches/".$val."?api_key=";
    //         $request_url = $url.$api_key;
    //         $curl = curl_init($request_url);
    //         curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    //         $response_json = curl_exec($curl);
    //         $response =  json_decode($response_json);
    //         $game_time = $response->info->game_length;

            
    //         foreach($response->info->participants as $value){
    //             if($value->puuid === $puuid){
    //                 $placement = $value->placement;
    //                 $level = "Level ".$value->level;
    //                 $placement_css_class = "";

    //                 if($placement === 1){
    //                     $placement_css_class = "gold";
    //                 }else if($placement === 2){
    //                     $placement_css_class = "silver";
    //                 }else if($placement === 3){
    //                     $placement_css_class = "bronze";
    //                 }else{
    //                     $placement_css_class = "iron";
    //                 }

    //                 $match_list = $match_list.'<div class="match-wrapper">
    //                     <div class="side-bar side-bar-'.$placement_css_class.'"></div>
    //                     <div class="game-info">
    //                     <h2 class="placement-'.$placement_css_class.'">#'.$placement.'</h2>';

    //                 $match_list = $match_list.'<span>Ranked</span>
    //                                         <span>'.gmdate("i:s", $game_time).'</span>
    //                                         <span>'.$level.'</span>
    //                                         </div>
    //                                         <div class="details">
    //                                             <div class="traits-list">';

    //                 foreach($value->traits as $tier){
    //                     $tier_total = $tier->tier_total;
    //                     $current_tier = $tier->tier_current;
    //                     if($tier_total === 1){
    //                         if($current_tier == 1){
    //                             $tier->style = 1;
    //                         }
    //                     }else if($tier_total === 2){
    //                         if($current_tier === 1){
    //                             $tier->style = 3;
    //                         }else if($current_tier === 2){
    //                             $tier->style = 1;
    //                         }
    //                     }else if($tier_total === 3){
    //                         if($current_tier === 1){
    //                             $tier->style = 3;
    //                         }else if($current_tier === 2){
    //                             $tier->style = 2;
    //                         }else if($current_tier === 3){
    //                             $tier->style = 1;
    //                         }
    //                     }

    //                 }

    //                 usort($value->traits, function($a, $b){
    //                     if($a->style < $b->style){
    //                         return -1;
    //                     }else if($a->style > $b->style){
    //                         return 1;
    //                     }
    //                     return 0;
    //                 });

    //                 foreach($value->traits as $ind_trait){

    //                     $tier_total = $ind_trait->tier_total;
    //                     $current_tier = $ind_trait->tier_current;
    //                     $name = $ind_trait->name;
    //                     $needle = "_";
    //                     $trait_css_class = "";
    //                     if(strpos($name, $needle)){
    //                         $name = explode("_", $name);
    //                         $name = $name[1];
    //                     }
    //                     if($tier_total === 1){
    //                         if($current_tier === 1){
    //                             $trait_css_class = "third";
    //                         }else if($current_tier === 0){
    //                             continue;
    //                         }
    //                     }else if($tier_total === 2){
    //                         if($current_tier === 1){
    //                             $trait_css_class = "first";
    //                         }else if($current_tier === 2){
    //                             $trait_css_class = "third";
    //                         }else if($current_tier === 0){
    //                             continue;
    //                         }
    //                     }else if($tier_total === 3 || $tier_total === 4){
    //                         if($current_tier === 1){
    //                             $trait_css_class = "first";
    //                         }else if($current_tier === 2){
    //                             $trait_css_class = "second";
    //                         }else if($current_tier === 3){
    //                             $trait_css_class = "third";
    //                         }else if($current_tier === 4){
    //                             $trait_css_class = "forth";
    //                         }else if($current_tier === 0){
    //                             continue;
    //                         }
    //                     }

    //                     $match_list = $match_list.'<div class="trait '.$trait_css_class.'-bonus">
    //                                                         <img class="trait-img" src = "images/synergies/'.$name.'.png" >
    //                                             </div>';
    //                 }

    //                 $match_list = $match_list.'</div>
    //                 <div class="champions-list">';

                
    //                 foreach($value->units as $unit){
    //                     $star = $unit->tier;
    //                     $character_id = explode("_",$unit->character_id);
    //                     if($character_id[1] === "LuxWind"){
    //                         $character_id[1] = "LuxCloud";
    //                     }else if($character_id[1] === "QiyanaWoodland"){
    //                         $character_id[1] = "QiyanaMountain";
    //                     }
    //                     $match_list = $match_list.'<div class = "champion">
    //                     <img class="champ_img" src="images/champions/'.$character_id[1].'.png" >
    //                     <img class="stars" src="images/general/stars'.$star.'.png" >
    //                 </div>';

    //                 }
    //                 $match_list = $match_list.' 
    //                         </div>
    //                     </div>    
    //                 </div>';
            
                    
    //             };
    //         }
    //         echo $match_list;
    //     }
        
    // };
    
    ?>
</div>
</main>

<script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src = "scripts/nav-bar.js"></script>
<script src = "scripts/match-history.js"></script>
</body>
</html>