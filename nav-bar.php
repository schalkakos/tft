<nav>
    <div class="nav-wrapper">
        <a class="logo-wrapper" href="index.php">
            <div class="nav-logo-wrapper">
                <img class="nav-logo" src="images\general\Tft_icon.ico" alt="">
                <h2 class="nav-title">TF Tactics</h2>
            </div>
        </a>
        <ul class="nav-links">
            <li class="nav-item"><a href="match-history.php" class="nav-link">Match History</a></li>
            <li class="nav-item"><a href="champions.php" class="nav-link">Champions</a></li>
            <li class="nav-item"><a href="team-comps.php" class="nav-link">Team Comps</a></li>
            <li class="nav-item"><a href="item-builder.php" class="nav-link">Item Builder</a></li>
        </ul>
        <img class="menu-icon" src="images/general/menu-icon.png" >
    </div>
</nav>