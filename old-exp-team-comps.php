<?php
include("team_comp.class.php");
    
$team = new TeamComp();

function loadInitial()

        function display_team($team_id){
            $champ_array = $this->load_team_comp_data($team_id);
            $syn_ids = $this->load_synergies_ids($team_id);
            $syn_array =  $this->load_active_synergies($syn_ids);
            $this->load_characters($champ_array);


            $grade = $this->grade;
            $t_id = $this->team_id;
            $team_name = $this->team_name;
            $needle = " ";

            $html = '
            <div class="team-comp-wrapper" id='.$t_id.'>
                <div class="basic-info">
                    <div class="title">
                        <div class="grade"><span>'.$grade.'</span></div>
                        <div><span>'.$team_name.'</span></div>
                    </div>
                    <div class="team-members">';
                    

            foreach($this->characters as $key=>$value){
                if($value["champ_name"]){
                    $display_name = $value["champ_name"];
                    
                    if(strpos($needle, $display_name)){
                        $champ_name = str_replace(" ", "-", $display_name);
                    }else{
                        $champ_name = $display_name;
                    }
                    $img_url = $value["img_url"];

                    
                            $html .= '<div champions-name='.$champ_name.'>
                                <img class="team-member-img" src='.$img_url.'>
                                </div>';
                    }
            }

            $html .= '</div>
            <div class="trait-assist-list">';


            foreach($syn_array as $value){
                if(strpos($needle, $value)){
                    $value = str_replace(" ", "-", $value);
                }
                $html .= '<div class="trait-assist" synergies-name='.$value.'></div>';

            }
        $html .= '</div><img class="chevron" src="images/general/down-chevron.svg">
        </div></div>';

        return $html;
        } 
   }

   $team_id_query = $db->prepare("SELECT
                id
                FROM team_comps");
                $team_id_query->execute();
                $result = $team_id_query -> fetchAll(PDO::FETCH_ASSOC);
                // echo "test: ".$result[1]["id"];
                $team_comp = new team_comp;
                // var_dump($result);
                foreach($result as $key=>$value){
                    // echo "key: ".$key;
                    // echo "value: ".$value["id"];
                    
                    echo $team_comp->display_team($value["id"]);
                    // print_r($team_comp->debug());
                }




   
