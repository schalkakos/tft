<?php


   $servername = "localhost";
   $username = "root";
   $password = "";
   $db_name="tftclone";
   
   // Create connection
   $db = new PDO("mysql:host=$servername;dbname=$db_name",$username,$password);
    
    
   $team_comp_query = $db->prepare(
    "SELECT
    team_comps.id AS team_comp_id,
    team_comps.team_comp_name AS comp_name,
    champ_1.img_url AS champ_1_img,
    champ_2.img_url AS champ_2_img,
    champ_3.img_url AS champ_3_img,
    champ_4.img_url AS champ_4_img,
    champ_5.img_url AS champ_5_img,
    champ_6.img_url AS champ_6_img,
    champ_7.img_url AS champ_7_img,
    champ_8.img_url AS champ_8_img,
    champ_1.champ_name AS champ_1_name,
    champ_2.champ_name AS champ_2_name,
    champ_3.champ_name AS champ_3_name,
    champ_4.champ_name AS champ_4_name,
    champ_5.champ_name AS champ_5_name,
    champ_6.champ_name AS champ_6_name,
    champ_7.champ_name AS champ_7_name,
    champ_8.champ_name AS champ_8_name,
    syn1.syn_name AS syn_1_name,
    syn2.syn_name AS syn_2_name,
    syn3.syn_name AS syn_3_name,
    syn4.syn_name AS syn_4_name,
    syn5.syn_name AS syn_5_name,
    syn6.syn_name AS syn_6_name,
    syn7.syn_name AS syn_7_name,
    team_comps.grade AS grade
    FROM team_comps
    LEFT JOIN champions AS champ_1
    ON team_comps.champ_1_id = champ_1.champ_id
    LEFT JOIN champions AS champ_2
    ON team_comps.champ_2_id = champ_2.champ_id
    LEFT JOIN champions AS champ_3
    ON team_comps.champ_3_id = champ_3.champ_id
    LEFT JOIN champions AS champ_4
    ON team_comps.champ_4_id = champ_4.champ_id
    LEFT JOIN champions AS champ_5
    ON team_comps.champ_5_id = champ_5.champ_id
    LEFT JOIN champions AS champ_6
    ON team_comps.champ_6_id = champ_6.champ_id
    LEFT JOIN champions AS champ_7
    ON team_comps.champ_7_id = champ_7.champ_id
    LEFT JOIN champions AS champ_8
    ON team_comps.champ_8_id = champ_8.champ_id
    LEFT JOIN  synergies AS syn1
    ON team_comps.syn_1_id = syn1.syn_id
    LEFT JOIN  synergies AS syn2
    ON team_comps.syn_2_id = syn2.syn_id
    LEFT JOIN  synergies AS syn3
    ON team_comps.syn_3_id = syn3.syn_id
    LEFT JOIN  synergies AS syn4
    ON team_comps.syn_4_id = syn4.syn_id
    LEFT JOIN  synergies AS syn5
    ON team_comps.syn_5_id = syn5.syn_id
    LEFT JOIN  synergies AS syn6
    ON team_comps.syn_6_id = syn6.syn_id
    LEFT JOIN  synergies AS syn7
    ON team_comps.syn_7_id = syn7.syn_id
    ORDER BY grade");

    function displayCharacters($res){
        $team_members = "";
        for($i = 1; $i <= 8; $i++){
            $img_url = "champ_".$i."_img";
            $name = "champ_".$i."_name";
            $needle = " ";
            if( strpos($res[$name], $needle)){
                $res[$name] = str_replace(" ", "-", $res[$name]);
            }
            if($res[$img_url]){
                $team_members = $team_members.'<div champions-name='.$res[$name].'><img class="team-member-img"  src="'.$res[$img_url].'"></div>';
            }
        }
        return $team_members;
    }

    function displayTraitAssist($res){
        $traits = "";
        for($i = 1; $i <= 7; $i++){
            $needle = " ";
            $trait_name = "syn_".$i."_name";
            if( strpos($res[$trait_name], $needle)){
                $res[$trait_name] = str_replace(" ", "-", $res[$trait_name]);
            }
            if($res[$trait_name]){
                $traits = $traits. '<div class="trait-assist" synergies-name='.$res[$trait_name].'></div>' ;
            }
            
        }
        return $traits;
    }

    $team_comp_query->execute();
    $result = $team_comp_query -> fetchAll(PDO::FETCH_ASSOC);
    // print_r($result);
    foreach($result as $i => $array){
        echo '<div class="team-comp-wrapper" id='.$array["team_comp_id"].'>
                <div class="basic-info">
                    <div class="title">
                        <div class="grade"><span>'.$array["grade"].'</span></div>
                        <div><p>'.$array["comp_name"].'</p></div>
                    </div>
                    <div class="team-members">';

                echo displayCharacters($array);
        echo        '</div>
                    <div class="trait-assist-list">';
                echo displayTraitAssist($array);
                    echo '</div>
                    <img class="chevron" src="images/general/down-chevron.svg">
                </div>
            </div>';
    }
    
    
            
    ?>