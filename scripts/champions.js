$(document).ready(function(){

    $(".filter-title").click(function(){
        let cur_chevron=$(this).find("img.chevron");
        let list_element=cur_chevron.parent().siblings();
        
        if(!cur_chevron.hasClass("chevron-open")){
            //cur_chevron.css("transform", "rotate(180deg)");
            cur_chevron.addClass("chevron-open");
            list_element.addClass("filter-items-dp");;
         }else if(cur_chevron.hasClass("chevron-open")){
            //cur_chevron.css("transform", "rotate(0deg)");
            cur_chevron.removeClass("chevron-open");
            list_element.removeClass("filter-items-dp");
        }
    });


    
    $(".filter-title").click(function(){
        $(this).find("span").toggleClass("active");
    });

    
    let terms_obj={
        cost:[],
        origin:[],
        tfclass:[],
    };

    function getAllSelectedFilters(){
        let temp_array=[];
        for (let key in terms_obj) {
            if(!terms_obj[key].length==0){
                temp_array.push(key);
            }
        }
        return temp_array;
    }


    function applyActiveFilter(current_champ_wrapper,e, temp_array){
        e=0;
       
        if(!temp_array.length==0){
            temp_array.forEach(function(key){
                current_champ_wrapper.find('div.' + key + '-sub').each(function(){
                
                    let search_term=$(this).attr("search-term");
                    //in each array iterates through every value
                    terms_obj[key].forEach(function(value){

                        //check if the filter and the list value match
                        if(value==search_term){
                            
                            //variable for checking if an element is hidden or not
                            e++;
                        }
                    });
                });
            });

            if(e<temp_array.length){
                current_champ_wrapper.addClass("filter-dn");

            }
        }
    }



    $("li.filter-item").click(function(){
        //make the text opacity 100% and the circle full
        $(this).find(".circle").toggleClass("circle-active");
        $(this).find("span").toggleClass("active");        
        //get the catergory of the clicked element(cost, origin, class)
        let category=$(this).closest("ul").attr("val-type")        
        //boolean variable for checking whether or not a filter is active
        let act_filt=$(this).find("span").hasClass("active");
        //get the value of the clicked filter
        let search_val=$(this).attr("name");


        $(".filter-dn").removeClass("filter-dn");
        if(act_filt){
            //push the filter value to the right array in the object
            terms_obj[category].push(search_val);
            let e=0;
            let temp_array=getAllSelectedFilters()
            $(".champ-wrapper").each(function(){
                applyActiveFilter($(this),e, temp_array)
            });

            $(".display-filters").append(`<div class="display-filter-wrapper" filter-value="${search_val}"><div class="display-filter-value">${search_val}</div><img src="images/general/cancel.svg" class="display-filter-close"></div>`);


        }else if(!act_filt){

            let length=terms_obj[category].length;
            for(let i =0;i<length;i++){        
                if(terms_obj[category][i]===search_val){
                    terms_obj[category].splice(i, 1 );
                }
            }
            let e=0;
            let temp_array=getAllSelectedFilters()
            $(".champ-wrapper").each(function(){
                applyActiveFilter($(this),e, temp_array)
            });


            $(`div[filter-value=${search_val}]`).remove();
        }
    });

    $(".search-bar").keyup(function(e){
        if (e.which <= 90 && e.which >= 48 || e.which == 8 || e.which == 46){
            $("div.champ-wrapper").removeClass("search-bar-dn");
            let search_bar_val=$(this).val();
            let i = 0;
            $("div.champ-wrapper").each(function(){
                i=0;
                $(this).find(`div[search-term]`).each(function(){
                    let search_term = $(this).attr("search-term").toLowerCase();
                    if(search_term.indexOf("-")){
                        search_term = search_term.replace("-", " ")
                    }
                    //console.log(`vals value ${val}`)
                    if(!search_term.includes(search_bar_val)){
                        
                        //console.log("doesn't contain")
                        
                    }else if(search_term.includes(search_bar_val)){
                        i++;
                        return false;
                    };
                });
                if(!i){
                    $(this).closest("div.champ-wrapper").addClass("search-bar-dn");
                }
                if(search_bar_val==0){
                    $("div[style]").removeAttr("style");
                };
            });
        };
    });

    $(".search-bar").on("focus focusout" ,function(){
        $(".search-bar-wrapper").toggleClass("focused")
    })


    $(".content").on("click", "div.display-filter-wrapper", function(){
        $(this).remove();
        let search_term=$(this).attr("filter-value");      
        let category=$(`li[name='${search_term}']`).closest("ul").attr("val-type");
        $(`li[name=${search_term}]`).find(".filter-span").removeClass("active");
        $(`li[name=${search_term}]`).find(".circle").removeClass("circle-active")
        let length=terms_obj[category].length
            for(let i =0;i<length;i++){        
                if(terms_obj[category][i]===search_term){
                    terms_obj[category].splice(i, 1 );
                }
            }
        let e=0;
        let temp_array=getAllSelectedFilters();
        $(".filter-dn").removeClass("filter-dn");
        $(".champ-wrapper").each(function(){
            applyActiveFilter($(this),e, temp_array);
        });       
    });

    $(".reset-button").click(function(){
        $(".filter-dn").removeClass("filter-dn");
        $(".display-filter-wrapper").remove();
        
        for (let key in terms_obj) {
            terms_obj[key]=[];
        }

        $("li > span.active").removeClass("active");
        $("div.circle-active").removeClass("circle-active")
    })


    $(".list-title").click(function(){
        let clicked_again=$(this).hasClass("active-sort");
        let order_by="";
        let direction="";

        if(clicked_again){
            $(this).find("img.sort-img-down").toggleClass("active-sort-img");
            $(this).find("img.sort-img-up").toggleClass("active-sort-img");
            order_by=$(this).attr("sort-value");
           //order_by= order_by+ " " + $(".active-sort-img").attr("sort-type");
           direction=$(".active-sort-img").attr("sort-type");

        }else{
            $(".active-sort").removeClass("active-sort");
            $(".active-sort-img").removeClass("active-sort-img");
            $(this).addClass("active-sort");
            $(this).find("img.sort-img-down").addClass("active-sort-img");
            order_by=$(this).attr("sort-value");
            //order_by= order_by+ " " + $(".active-sort-img").attr("sort-type");
            direction=$(".active-sort-img").attr("sort-type");


        }
        $(".content-list").empty();

        $.ajax({
            url: "champions-ajax.php" ,
            dataType: "json",
            type: "GET",
            data: {"orderBy": order_by, "direction":direction},
            success: function(response){
                let resp_obj=response
                for(let key in resp_obj){
                    $(".content-list").append(function(){
                        if(resp_obj[key]["origin2"]){
                            return  `<div class="champ-wrapper">                            
                            <div class="name" search-term=${resp_obj[key]["champ_name"]}>
                                <img class="champ-img" src=${resp_obj[key]["img_url"]} alt=${resp_obj[key]["champ_name"]}>
                                <a href="#" class="champ-wrapper-span">${resp_obj[key]["champ_name"]}</a>
                            </div>
                            <div class="origin">
                                <div class="origin-sub" search-term=${resp_obj[key]["origin"]}>
                                    <img class="champ-origin-img" src=${resp_obj[key]["origin_url"]} alt=${resp_obj[key]["origin"]}>
                                    <span class="champ-wrapper-span">${resp_obj[key]["origin"]}</span>
                                </div>
                                <div class="origin-sub" search-term=${resp_obj[key]["origin2"]}>
                                    <img class="champ-origin-img" src=${resp_obj[key]["origin2_url"]} alt=${resp_obj[key]["origin2"]}>
                                    <span class="champ-wrapper-span">${resp_obj[key]["origin2"]}</span>
                                </div>
                            </div>
                            <div class="tfclass">
                                <div class="tfclass-sub" search-term=${resp_obj[key]["tfclass"]}>
                                    <img class="champ-tfclass-img" src=${resp_obj[key]["tfclass_url"]} alt=${resp_obj[key]["tfclass"]}>
                                    <span class="champ-wrapper-span">${resp_obj[key]["tfclass"]}</span>
                                </div>
                            </div>
                            <div class="cost-sub" search-term=${resp_obj[key]["cost"]}>
                                    <img class="coin" src="images/general/coin.png" alt="coin">
                                    <span class="champ-wrapper-span">${resp_obj[key]["cost"]}</span>
                            </div>
                        </div>`
                        }else if(resp_obj[key]["tfclass2"]){
                            return `<div class="champ-wrapper">
                            <div class="name" search-term=${resp_obj[key]["champ_name"]}>
                                <img class="champ-img" src=${resp_obj[key]["img_url"]} alt=${resp_obj[key]["champ_name"]}>
                                <a href="#" class="champ-wrapper-span">${resp_obj[key]["champ_name"]}</a>
                            </div>
                            <div class="origin">
                            <div class="origin-sub" search-term=${resp_obj[key]["origin"]}>
                            <img class="champ-origin-img" src=${resp_obj[key]["origin_url"]} alt=${resp_obj[key]["origin"]}>
                            <span class="champ-wrapper-span">${resp_obj[key]["origin"]}</span>
                        </div>
                            </div>
                            <div class="tfclass">
                                <div class="tfclass-sub" search-term=${resp_obj[key]["tfclass"]}>
                                    <img class="champ-tfclass-img" src=${resp_obj[key]["tfclass_url"]} alt=${resp_obj[key]["tfclass"]}>
                                    <span class="champ-wrapper-span">${resp_obj[key]["tfclass"]}</span>
                                </div>
                                <div class="tfclass-sub" search-term=${resp_obj[key]["tfclass2"]}>
                                    <img class="champ-tfclass-img" src=${resp_obj[key]["tfclass2_url"]} alt=${resp_obj[key]["tfclass2"]}>
                                    <span class="champ-wrapper-span">${resp_obj[key]["tfclass2"]}</span>
                                </div>
                            </div>
                            <div class="cost-sub" search-term=${resp_obj[key]["cost"]}>
                                    <img class="coin" src="images/general/coin.png" alt="coin">
                                    <span class="champ-wrapper-span">${resp_obj[key]["cost"]}</span>
                            </div>
                        </div>`
                        }else{
                            return `<div class="champ-wrapper">                            
                            <div class="name" search-term=${resp_obj[key]["champ_name"]}>
                                <img class="champ-img" src=${resp_obj[key]["img_url"]} alt=${resp_obj[key]["champ_name"]}>
                                <a href="#" class="champ-wrapper-span">${resp_obj[key]["champ_name"]}</a>
                            </div>
                            <div class="origin">
                                <div class="origin-sub" search-term=${resp_obj[key]["origin"]}>
                                    <img class="champ-origin-img" src=${resp_obj[key]["origin_url"]} alt=${resp_obj[key]["origin"]}>
                                    <span class="champ-wrapper-span">${resp_obj[key]["origin"]}</span>
                                </div>
                            </div>
                            <div class="tfclass">
                                <div class="tfclass-sub" search-term=${resp_obj[key]["tfclass"]}>
                                    <img class="champ-tfclass-img" src=${resp_obj[key]["tfclass_url"]} alt=${resp_obj[key]["tfclass"]}>
                                    <span class="champ-wrapper-span">${resp_obj[key]["tfclass"]}</span>
                                </div>
                            </div>
                            <div class="cost-sub" search-term=${resp_obj[key]["cost"]}>
                                    <img class="coin" src="images/general/coin.png" alt="coin">
                                    <span class="champ-wrapper-span">${resp_obj[key]["cost"]}</span>
                            </div>
                        </div>`
                        }
                    });

                }
                let e=0;
                let temp_array=getAllSelectedFilters()
                $(".champ-wrapper").each(function(){
                    applyActiveFilter($(this),e, temp_array)
                });
            }
        });
        
    })

    let timer;
    $(".content").on("mouseenter",".origin-sub, .tfclass-sub",function(){
        
        let synergie_name = $(this).attr("search-term");
        if(synergie_name.indexOf("-")){
            synergie_name = synergie_name.replace("-", " ");
        }
        let curr_el=$(this)
        timer=setTimeout(function(){
            $.ajax({
                url: "champions-ajax.php" ,
                dataType: "json",
                type: "GET",
                data: {"synergie-name": synergie_name},
                success: function(response){
                    let syn_res=response
                    curr_el.append(function(){
                        let element_to_be_added=`<div class="syn-det-wrapper">
                            <div class="syn-det-title">
                                <img class="syn-det-title-img" src=${syn_res[0]["syn_url"]} alt=${syn_res[0]["syn_name"]}>
                                <span>${syn_res[0]["syn_name"]}</span>
                            </div>
                            <div class="syn-det-desc-wrapper">`
                                if(syn_res[0]["syn_desc"]){
                                    element_to_be_added+=`<span class="syn-det-desc-title">Mages have a chance on cast to instead Doublecast.</span>`
                                }
                                if(syn_res[0]["syn_t1_numb"]){
                                element_to_be_added+=`<div class="syn-det-bonus">
                                    <div class="syn-det-bonus-number" id="first-bonus">
                                        <span>${syn_res[0]["syn_t1_numb"]}</span>
                                    </div>
                                    <div class="syn-det-bonus-val">
                                        <span >${syn_res[0]["syn_t1_bonus"]}</span>
                                    </div>
                                    </div>`
                                }
                                if(syn_res[0]["syn_t2_numb"]){
                                    element_to_be_added+=`<div class="syn-det-bonus">
                                        <div class="syn-det-bonus-number" id="second-bonus">
                                            <span>${syn_res[0]["syn_t2_numb"]}</span>
                                        </div>
                                        <div class="syn-det-bonus-val">
                                            <span >${syn_res[0]["syn_t2_bonus"]}</span>
                                        </div>
                                        </div>`
                                }
                                if(syn_res[0]["syn_t3_numb"]){
                                    element_to_be_added+=`<div class="syn-det-bonus">
                                        <div class="syn-det-bonus-number" id="third-bonus">
                                            <span>${syn_res[0]["syn_t3_numb"]}</span>
                                        </div>
                                        <div class="syn-det-bonus-val">
                                            <span >${syn_res[0]["syn_t3_bonus"]}</span>
                                        </div>
                                        </div>`
                                }
                                if(syn_res[0]["syn_t4_numb"]){
                                    element_to_be_added+=`<div class="syn-det-bonus">
                                        <div class="syn-det-bonus-number" id="third-bonus">
                                            <span>${syn_res[0]["syn_t4_numb"]}</span>
                                        </div>
                                        <div class="syn-det-bonus-val">
                                            <span >${syn_res[0]["syn_t4_bonus"]}</span>
                                        </div>
                                        </div>`
                                }
                                element_to_be_added+=`</div>
                                <div class="similar-champs-wrapper">
                                    <span> Other champions:</span>
                                    <div class="similar-champs-img">`
                                for(let key in syn_res[0]["other_img_url"]){
                                    if(syn_res[0]["other_img_url"][key]){
                                        element_to_be_added+=`<img src=${syn_res[0]["other_img_url"][key]} class="similar-champ">`
                                    }
                                }
                                element_to_be_added+=`</div></div>`
                                return element_to_be_added
                    })
                }
            })
    },200);

    })
    .on("mouseleave",".origin-sub, .tfclass-sub", function(){
        $(".syn-det-wrapper").remove();
        clearTimeout(timer);
    });

})