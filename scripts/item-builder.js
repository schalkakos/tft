$(document).ready(function(){
    
    
    $("div[id=1]").find("img").addClass("active");
    $(".item-img").click(function(){
        let item_id=$(this).attr("id");
        let item_type=$(this).attr("type");
        $(".active").removeClass("active");
        $(this).find("img").addClass("active");
        $.ajax({
            url: "item-builder-ajax.php",
            dataType: "json",
            type: "GET",
            data: {"item-id": item_id, "item-type":item_type},
            success: function(response){
                $(".item-wrapper").remove();
                for (let key in response) {
                    $(".item-builder-list").append(`<div class="item-wrapper">    
                    <div class="recipe">
                        <img class="result-img" src=${response[key]['basic_1_img']}>
                        <img class="result-img" src=${response[key]['basic_2_img']}>
                    </div>
                    <div class="result">
                        <img class="result-img" src=${response[key]['finished_img']}>
                    </div>
                    <div class="description">
                        <p>${response[key]['finished_desc']}</p>
                    </div>
                </div>`)
                }
            }

        })
    });

    let timer
    $(".content").on("mouseenter", ".item-img, .recipe-list",function(){
        let item_id=$(this).attr("id");
        let item_type=$(this).attr("type");
        let that=$(this)
        timer=setTimeout(function(){
            $.ajax({
                url: "item-builder-ajax.php",
                dataType: "json",
                type: "GET",
                data: {"hover-item-id": item_id, "hover-item-type": item_type},
                success: function(response){
                    $(that).append(function(){
                        let base_response="";
                        if(item_type==="base"){
                            base_response=`<div class="item-det-wrapper">
                                <div class="det-info">
                                    <img class="det-img" src=${response[0]["item_url"]} alt="">
                                    <div class="det-stats">
                                        <span>${response[0]["item_name"]}</span>
                                        <div class="det-bonus">
                                            <span>${response[0]["item_bonus_1"]}</span>
                                            <span>${response[0]["item_bonus_2"]}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="det-recipe">
                                    <span>Into:</span>`
                                    for (let key in response[0]["item_recipe"]) {
                                        base_response+=`<img src=${response[0]["item_recipe"][key]} >`
                                    }
                                    // response[0].item_recipe.forEach(element => {
                                    //     base_response+=`<img src=${element} >`
                                    // });

                        base_response+=`</div>
                                    </div>`;

                                    return base_response;
                        }else if(item_type==="finished"){
                            return `<div class="item-det-wrapper">
                                <div class="det-info">
                                    <img class="det-img" src=${response[0]["item_url"]} alt="">
                                    <div class="det-stats">
                                        <span>${response[0]["item_name"]}</span>
                                        <div class="det-bonus">
                                            <span>${response[0]["item_bonus_1"]}</span>
                                            <span>${response[0]["item_bonus_2"]}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="det-desc"><p>${response[0]["item_desc"]}</p></div>
                                <div class="det-recipe">
                                    <span>Recipe:</span>
                                    <img src=${response[0]["item_recipe"]["basic_1_img"]}>
                                    <img src=${response[0]["item_recipe"]["basic_2_img"]}>
                                </div>
                            </div>`
                        }
                    })

                }
            })
        },300);

    })
    .on("mouseleave",".item-img, .recipe-list", function(){
        clearTimeout(timer);
        $(".item-det-wrapper").remove();
    } )
    
})




/*`<div class="item-det-wrapper">
<div class="det-info">
    <img class="det-img" src="images/items/finished/Zekes_Herald.png" alt="">
    <div class="det-stats">
        <span>Zeke's Herald</span>
        <div class="det-bonus">
            <span>+15 Attack Damage</span>
            <span>+200 Health</span>
        </div>
    </div>
</div>
<div class="det-desc"><p>When combat begins, the wearer and all allies within 2 hexes in the same row gain +15% Attack Speed for the rest of combat.</p></div>
<div class="det-recipe">
    <span>Recipe:</span>
    <img src="images/items/base/B._F._Sword.png" >
    <img src="images/items/base/Giants_Belt.png" alt="">
</div>
</div>`*/