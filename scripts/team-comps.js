$(document).ready(function(){
    



    $(".filter-img-list").on("click", "img.filter-img",function(){

        if($(this).hasClass("active-filter-img")){
            $(this).removeClass("active-filter-img");
            $(".team-comp-wrapper").show();
        }else{
            $(".active-filter-img").removeClass("active-filter-img")
            $(this).addClass("active-filter-img");
        
        
        
            let img_term = $(this).attr("name");

            let category = $(".active-tab").attr("category")
            $(".team-comp-wrapper").show();
            $(".team-comp-wrapper").each(function(){

                let i=0;
                $(this).find(`div[${category}-name]`).each(function(){
                    
                    let current_name=  $(this).attr(`${category}-name`);
                    if(current_name === img_term){
                        i++;
                    }
                    
                })
                if(!i){
                    $(this).hide();
                }
            })

        }
    })

    $(".search-bar").keyup(function(e){
        if(e.which <= 90 && e.which >= 48 || e.which == 8 || e.which == 46){
            $(".filter-img").show();
            let search_val = $(this).val();
            $(".filter-img").each(function(){
                let img_term = $(this).attr("name").toLowerCase();

                if(!img_term.includes(search_val)){
                    $(this).hide();
                }
            })

        }
    })

    $(".tab").click(function(){
        $(".active-tab").removeClass("active-tab");
        $(".search-bar").val("");
        $(".team-comp-wrapper").show();
        $(this).addClass("active-tab");
        let category = $(this).attr("category");
        $.ajax({
            url: "team-comps-ajax.php",
            type: "GET",
            data:{ "category": category},
            dataType: "json",
            success: function(response){
                let img_list = "";
                for (const key in response) {
                    let img_url = response[key]["img_url"];
                    let name = response[key]["name"];
                    if(name.indexOf(" ")){
                        name = name.replace(" ", "-");
                    }
                    img_list += `<img class="filter-img" name=${name} src=${img_url}>`;
                    
                    
                        
                    
                }
                $(".filter-img").remove();
                $(".filter-img-list").append(img_list);
            }
        })
    })


    function synergies(resp){
        let trait_list = "";
        trait_list += `<div class="traits">
                            <div class="trait-title">
                                <h2>Synergies</h2>
                            </div>
                            <div class="trait-list">`

        for (let [key, value] of Object.entries(resp["synergie"])) {


            let syn_val = value['numb'];
            let syn_url = value['image'];
            let syn_name = value['name'];
            let syn_tier = value['tier'];
            trait_list = trait_list+`<div class="trait-details trait-${syn_tier}">
                                    <img class="trait-img " src=${syn_url} name=${syn_name}>
                                    <span>${syn_val}</span>
                                </div>`;
          }


        trait_list += "</div></div>"
        return trait_list;
    }

    function items(resp){
        let item_list = ` <div class="items">
                            <div class="item-title">
                                <h2>Important Items</h2>
                            </div>
                            <div class="item-list">`;

        for (let [key, value] of Object.entries(resp["character_items"])) {
            let champ_name = value["character"]["champ_name"];
            let champ_img = value["character"]["img_url"];
            item_list += `<div class="item-details">
                            <img class="team-member-img" src=${champ_img} name=${champ_name}>
                            <span> > </span>`;
            for(let i = 1; i <= 3; i++){
                
                
                // console.log(1);
                // console.log(value["item"][i]);
                if(value["item"][i]){
                    let item = value["item"][i]["item_url"];
                    item_list += `<img class="item" src=${item}>`;
                }
                
            }
            item_list += `</div>`; 

        }
        item_list +=`</div>`;
        return item_list;
    }

    $(".team-comp-wrapper").click(function(){
        console.log("1");
        let current_team_comp = $(this);
        if($(this).find("div.description").length){
            $(this).find("div.description").toggleClass("open").toggleClass("closed");
        }else{
            if($(this).attr("id")){
                let team_id = $(this).attr("id");
                $.ajax({
                    url: "team-comps-ajax.php",
                    type: "GET",
                    data:{ "team_id": team_id},
                    dataType: "json",
                    success: function(response){
                        console.log("2");
                        
                        let trait_list = synergies(response);
                        let item_list = items(response);
                        
                        current_team_comp.append(`<div class="description open">${trait_list}${item_list}</div>`);
                        
                        
                    }
                })
                
            }   
        }
    })




})

