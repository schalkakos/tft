<?php

 class Synergy {
    private $db;


    public function __construct(){
        include("connect.php");
        $this->db = new PDO("mysql:host=$servername;dbname=$db_name",$username,$password);

    
    }

    public function load($synergy_id){

        $query = $this->db->prepare("SELECT *
        FROM synergies 
        WHERE syn_id=:synId");
        $query->bindParam(":synId", $synergy_id);
        $query->execute();
        $result = $query -> fetchAll(PDO::FETCH_ASSOC);


        return $result[0];
    }

    public function loadLess($synergy_id){
        $query = $this->db->prepare("SELECT 
        syn_name,
        syn_url,
        syn_type
        FROM synergies 
        WHERE syn_id=:synId");
        $query->bindParam(":synId", $synergy_id);
        $query->execute();
        $result = $query -> fetchAll(PDO::FETCH_ASSOC);


        return $result[0];
    }

    public function characterSynergies($character_id){
        $query = $this->db->prepare("SELECT 
        or_id_1 AS origin_1,
        or_id_2 AS origin_2,
        cl_id_1 AS class_1,
        cl_id_2 AS class_2
        FROM champ_joint 
        WHERE champ_id=:champId");
        $query->bindParam(":champId", $character_id);
        $query->execute();
        $result = $query -> fetchAll(PDO::FETCH_ASSOC);
        $i = 0;
        foreach($result[0] as $k=>$v){
            // $this->$k = $v;
            

            if($v){
                $synergie_data = $this->load($v);
                $this->synergie[$i] = $synergie_data;
                $i++;
            }
        }
        return $this;
    }

    
    public function activeSynergies($syn_id, $syn_val){
        $query = $this->db->prepare("SELECT 
        syn_name,
        syn_url,
        syn_t1_numb AS t1,
        syn_t2_numb AS t2,
        syn_t3_numb AS t3
        FROM synergies 
        WHERE syn_id=:synId");
        $query->bindParam(":synId", $syn_id);
        $query->execute();
        $result = $query -> fetchAll(PDO::FETCH_ASSOC);

        $this->name = $result[0]["syn_name"];
        $this->image = $result[0]["syn_url"];
        $this->numb = $syn_val;

        $first_tier = $result[0]["t1"];   
        $second_tier = $result[0]["t2"];
        $third_tier = $result[0]["t3"];
        
        if($first_tier && !$second_tier){
            $this->tier = "gold";
            $this->tier = "gold";
        }else if($second_tier && !$third_tier){
            if($syn_val >= $first_tier && $syn_val < $second_tier){
                $this->tier = "bronze";
            }else if($syn_val >= $second_tier){
                $this->tier = "gold";
            }
        }else if($third_tier){
            if($syn_val >= $first_tier && $syn_val < $second_tier){
                $this->tier = "bronze";
            }else if($syn_val >= $second_tier && $syn_val < $third_tier){
                $this->tier = "silver";
            }else if($syn_val >= $third_tier){
                $this->tier = "gold";
            }
        }
        return $this;
    }
 }
 