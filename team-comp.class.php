<?php
    include("character.class.php");
    include("item.class.php");
    include("synergy.class.php");


 class TeamComp {
    private $db;

    public function __construct(){
        include("connect.php");
        $this->db = new PDO("mysql:host=$servername;dbname=$db_name",$username,$password);

    
    }

    public function loadInitial($team_comp_id){
        
        $synergie = new Synergy();

        $query = $this->db->prepare("SELECT 
        id,
        team_comp_name,
        champ_1_id,
        champ_2_id,
        champ_3_id,
        champ_4_id,
        champ_5_id,
        champ_6_id,
        champ_7_id,
        champ_8_id,
        syn_1_id,
        syn_2_id,
        syn_3_id,
        syn_4_id,
        syn_5_id,
        syn_6_id,
        syn_7_id,
        grade
        FROM team_comps 
        WHERE id=:teamCompId");
        $query->bindParam(":teamCompId", $team_comp_id);
        $query->execute();
        $result = $query -> fetchAll(PDO::FETCH_ASSOC);
        
        $this->id = $result[0]["id"];
        $this->name = $result[0]["team_comp_name"];
        $this->grade = $result[0]["grade"];

        for($i = 1; $i <=8; $i++){
            $character = new Character();
            if($result[0]["champ_".$i."_id"]){
               
                $this->characters[$i] = $character->loadLess($result[0]["champ_".$i."_id"], false);

            }

        }

        for($e = 1; $e <=7 ; $e++){
            if($result[0]["syn_".$e."_id"]){
                $this->synergies[$e] = $synergie->loadLess($result[0]["syn_".$e."_id"]);
            }
        }


        return $this;
    }

    public function loadAjax($team_comp_id, $less_data = false){
        $query = $this->db->prepare("SELECT
        team_comp_item_id_1,
        team_comp_item_id_2,
        team_comp_item_id_3,
        team_comp_item_id_4,
        team_comp_item_id_5,
        team_comp_item_id_6,
        syn_1_id,
        syn_1_val,
        syn_2_id,
        syn_2_val,
        syn_3_id,
        syn_3_val,
        syn_4_id,
        syn_4_val,
        syn_5_id,
        syn_5_val,
        syn_6_id,
        syn_6_val,
        syn_7_id,
        syn_7_val
        from team_comps
        WHERE id=:teamId");
        $query->bindParam(":teamId", $team_comp_id);
        $query->execute();
        $result = $query -> fetchAll(PDO::FETCH_ASSOC);
        
        for($i = 1; $i <= 6; $i++){
            $character = new Character();
            
            if($result[0]["team_comp_item_id_".$i]){

                $joint_ids = $this->getItemIds($result[0]["team_comp_item_id_".$i]);



                if($less_data){
                    $this->character_items[$i]["character"] = $character->loadLess($joint_ids["champion_id"]);
                  }else{
                    $this->character_items[$i]["character"] = $character->load($joint_ids["champion_id"]);
                  }


                for($e = 1 ; $e <= 3; $e++){
                    $item = new Item();
                    if($joint_ids["item_id_".$e]){
                        $this->character_items[$i]["item"][$e] = $item->load($joint_ids["item_id_".$e]);
                    }
                }
            }
        }

        for($i = 1; $i <= 7; $i++){
            $synergie = new Synergy();
            if($result[0]["syn_".$i."_id"]){
                $this->synergie[$i] = $synergie->activeSynergies($result[0]["syn_".$i."_id"], $result[0]["syn_".$i."_val"]);
            }
        } 

        return $this;
    }

    public function getItemIds($j_id){

        $query = $this->db->prepare("SELECT *
        from team_comp_item_joint
        WHERE team_comp_item_id=:teamCompItemId");
        $query->bindParam(":teamCompItemId", $j_id);
        $query->execute();
        $result = $query -> fetchAll(PDO::FETCH_ASSOC);

        return $result[0];

    }
 }


 