<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $db_name="tftclone";


    $db = new PDO("mysql:host=$servername;dbname=$db_name",$username,$password);


if(isset($_GET["category"])){
    $category = $_GET["category"];
    if($category === "champions"){
    $query = $db->prepare("SELECT img_url AS img_url,
                             champ_name AS name
                            FROM champions");
    }else if($category === "synergies"){
        $query = $db->prepare("SELECT syn_url AS img_url,
                                syn_name AS name
                                FROM synergies");
            
    }

    $query->execute();
    $response = $query->fetchAll(PDO::FETCH_ASSOC);
    // echo json_encode("1");
    echo json_encode($response);
}

if(isset($_GET["team_id"])){
    $team_id = $_GET["team_id"];
    include("team-comp.class.php");
    $team = new TeamComp();


    $response = $team->loadAjax($team_id, true);
    echo json_encode($response);


}
