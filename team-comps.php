<?php
   $servername = "localhost";
   $username = "root";
   $password = "";
   $db_name="tftclone";
   
   // Create connection
   $db = new PDO("mysql:host=$servername;dbname=$db_name",$username,$password);
    
    
   $champion_query = $db->prepare("SELECT champ_name, img_url
    FROM champions 
    ORDER BY champ_name");
    $champion_query->execute();
    $result= $champion_query -> fetchAll(PDO::FETCH_ASSOC);
   
    $champions_list="";
    foreach ($result as $key => $array) {
        $needle=" ";
        if(strpos($array["champ_name"], $needle)){
            $array["champ_name"] = str_replace(" ", "-", $array["champ_name"]);
        }
        $champions_list=$champions_list.'<img class="filter-img" name="'.$array["champ_name"].'" src="'.$array["img_url"].'">';
        
        
    };
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>TF Tactics</title>
    <link rel="icon" href="images\general\Tft_icon.ico" type="image/ico">
    <link rel="stylesheet" type="text/css" href="styles/nav-bar.css">
    <link rel="stylesheet" type="text/css" href="styles/team-comps.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
</head>
<body>
<?php include("nav-bar.php");?>

<main>
    <aside>
        <div>
            <div class="filter-tabs">
                <div class="tab active-tab" category="champions">
                    <span>Champions</span>
                </div>
                <div class="tab" category="synergies">
                    <span>Traits</span>
                </div>
            </div>
            <div class="search-bar-wrapper">
                    <img class="search-bar-icon" src="images/general/search.svg">
                    <input type="text" class="search-bar" placeholder="Search by name, origin, or class...">
            </div>
            <div class="filter-img-list">
                <?php echo $champions_list ?>

            </div>
        </div>
    </aside>
    <div class="team-comp-list">
    
    <?php 
    
        include("team-comp.class.php");
        $team = new TeamComp();
        $team_id_query = $db->prepare("SELECT id
        FROM team_comps
        ORDER BY id");
        $team_id_query->execute();
        $result= $team_id_query -> fetchAll(PDO::FETCH_ASSOC);
        $html = "";
        foreach($result as $key=>$value){
            $data = $team->loadInitial($value["id"]);

            
            // echo $data->name;
            $html .= '<div class="team-comp-wrapper" id='.$data->id.'>
            <div class="basic-info">
                <div class="title">
                    <div class="grade"><span>'.$data->grade.'</span></div>
                    <div><span>'.$data->name.'</span></div>
                </div>
                <div class="team-members">';
                

            foreach($data->characters as $key=>$value){
                if($data->characters[$key]->champ_name){
                    $needle = " ";
                    $display_name = $data->characters[$key]->champ_name;
                    
                    if(strpos($display_name, $needle)){
                        $champ_name = str_replace(" ", "-", $display_name);
                    }else{
                        $champ_name = $display_name;
                    }
                    $img_url = $data->characters[$key]->img_url;

                            $html .= '<div champions-name='.$champ_name .'>
                                <img class="team-member-img" src='.$data->characters[$key]->img_url.'>    
                                </div>';
                    }
            }

            $html .= '</div>
            <div class="trait-assist-list">';

            foreach($data->synergies as $key=>$value){
                if(strpos($data->synergies[$key]["syn_name"], $needle)){
                    $data->synergies[$key]["syn_name"] = str_replace(" ", "-", $data->synergies[$key]["syn_name"]);
                }
                $html .= '<div class="trait-assist" synergies-name='.$data->synergies[$key]["syn_name"].'></div>';

            }
            $html .= '</div><img class="chevron" src="images/general/down-chevron.svg">
            </div></div>';

        
        }
        
        echo $html;
     ?>
    </div>
</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="scripts/nav-bar.js"></script>
<script src="scripts/team-comps.js"></script>
</body>
</html>